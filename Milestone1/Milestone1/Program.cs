﻿// Tyler Martinez
//CST 227

using GameCell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone1
{
    class Program
    {

        static Grid myGrid = new Grid(10);
        static Boolean reveal = false;

        static void Main(string[] args)
        {
            printGrid(myGrid); //print the initial grid

            myGrid.activateCells(); // activates cells to be live with a 20% chance
            myGrid.countLive(); // counts each live cell next to the current cell
            Console.WriteLine("Reveal the Grid? Press any key to continue.");
            Console.ReadKey();
            Console.WriteLine();

            myGrid.revealGrid = true;
            printGrid(myGrid); // prints the new grid after being revealed.

            Console.ReadKey();



        }

        static public void printGrid(Grid myGrid)
        {
            //prints grid
            for (int i = 0; i < myGrid.Size; i++)
            {
                for (int j = 0; j < myGrid.Size; j++)
                {

                    if (myGrid.revealGrid)
                    {
                        if (myGrid.gameGrid[i,j].isLive) // if the cell is live it will print a "*"
                        {
                            Console.Write("*");
                        }

                        else
                        {
                            Console.Write(myGrid.gameGrid[i,j].liveNeighbors); // if the cell is not live it will print its number of neighbors that are live.
                        }
                    }
                    else
                    {
                        Console.Write("#"); // the text printed for the hidden board
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("=====================================");
        }
    }
}
