﻿// Tyler Martinez
//CST 227

using System;
using System.Collections.Generic;
using System.Text;

namespace GameCell
{
    public class Grid
    {
        public int Size { get; set; }

        public Cell[,] gameGrid;

        public Grid(int s)
        {
            Size = s;

            gameGrid = new Cell[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    gameGrid[i, j] = new Cell(i, j);
                }
            }
        }

        public void activateCells()
        {
            // activates random cells to contain mines with a 20% chance of activating
            var random = new Random();
            var rng = random.Next(0, 1);
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    if (random.NextDouble() < 0.2)
                    {
                        //set live
                        gameGrid[r, c].isLive = true;
                        gameGrid[r, c].liveNeighbors = 9;
                    }
                }
            }

        }

        public bool revealGrid { get; set; } = false;

        public void countLive()
        {
            //takes each cell and looks at the cell next to it to see if it is live. If that cell is live it will add 1 point to the currentcell.
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    if (r == 0 && c == 0) // prevents out of bounds for the element 0,0
                    {
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }

                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (r == Size-1 && c == Size-1) // prevents out of bounds for the element found at the end of the array
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (r == 0 ) // prevents out of bounds at the beginning of each array
                    {
                       if (c != Size-1)
                       {
                            if (gameGrid[r, c + 1].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                            if (gameGrid[r + 1, c + 1].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                       }
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (c == 0) // prevents out of bounds in the top/first array
                    {
                        if (r != Size - 1)
                        {
                            if (gameGrid[r + 1, c].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                            if (gameGrid[r + 1, c + 1].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                        }
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (r == Size-1) // prevents out of bounds at the end of each array
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (c == Size-1) // prevents out of bounds at the bottom/last array
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else // no issues about out of bounds
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                }
            }
        }
    }
}