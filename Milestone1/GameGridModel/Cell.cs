﻿// Tyler Martinez
//CST 227

using System;
using System.Collections.Generic;
using System.Text;

namespace GameCell
{
    public class Cell
    {
        public int RowNumber { get; set; } = -1;
        public int ColumnNumber { get; set; } = -1;

        public bool beenVisited { get; set; } = false;

        public bool isLive { get; set; } = false;

        public int liveNeighbors { get; set; } = 0;

        public Cell(int r, int c)
        {
            RowNumber = r;
            ColumnNumber = c;
        }
    }
}
