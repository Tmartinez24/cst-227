﻿using CarClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarShopConsoleApp
{
    class Program
    {
        static Store CarStore = new Store();
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Welcome to the car store. First create some cars and put them into the store inventory. You may then add them to your cart and checkout and recieve your total cost.");
            int action = chooseAction();
            while (action != 0)
            {
                switch (action)
                {
                    case 1:
                        Console.Out.WriteLine("Adding a new Car");
                        String carMake = "";
                        String carModel = "";
                        Decimal carPrice = 0;
                        int carYear = 0;
                        String carColor = "";

                        Console.Out.WriteLine("What is the car make?");
                        carMake = Console.ReadLine();
                        Console.Out.WriteLine("What is the car model?");
                        carModel = Console.ReadLine();
                        Console.Out.WriteLine("What is the car price?");
                        carPrice = int.Parse(Console.ReadLine());
                        Console.Out.WriteLine("What is the car's year?");
                        carYear = int.Parse(Console.ReadLine());
                        Console.Out.WriteLine("What is the car's color?");
                        carColor = Console.ReadLine();

                        Car newCar = new Car();
                        newCar.Make = carMake;
                        newCar.Model = carModel;
                        newCar.Price = carPrice;
                        newCar.Year = carYear;
                        newCar.Color = carColor;
                        CarStore.CarList.Add(newCar);
                        printStoreInventory(CarStore);
                        break;

                    case 2:
                        printStoreInventory(CarStore);

                        int choice = 0;
                        Console.Out.WriteLine("Whcih car would you like to add to your cart?");
                        choice = int.Parse(Console.ReadLine());
                        CarStore.ShoppingList.Add(CarStore.CarList[choice]);
                        printShoppingCart(CarStore);
                        break;

                    case 3:

                        printShoppingCart(CarStore);
                        Console.Out.WriteLine("Your total cost is ${0}", CarStore.checkout());
                        break;

                    default:
                        break;

                }
                action = chooseAction();
            }
        }

        static public int chooseAction()
        {
            int choice = 0;
            Console.Out.Write("Chose an action:\n(1) Add a Car\n(2) Add Item to Cart\n(3) Checkout\n(0) Exit");
            try
            {
                choice = int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.Out.Write("Inccorect Numerical Value");
            }
            return choice;
        }

        static public void printStoreInventory(Store carStore)
        {
            Console.Out.WriteLine("These are the cars in the inventory.");
            int i = 0;
            foreach (var c in carStore.CarList)
            {
                Console.Out.WriteLine(String.Format("Car # = {0} {1} ", i, c.Display));
                i++;
            }
        }

        static public void printShoppingCart(Store carStore)
        {
            Console.Out.WriteLine("These are the cars in your shopping cart.");
            int i = 0;
            foreach (var c in carStore.ShoppingList)
            {
                Console.Out.WriteLine(String.Format("Car # = {0} {1} ", i, c.Display));
                i++;
            }
        }

    }
}
