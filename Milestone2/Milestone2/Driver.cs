﻿// Tyler Martinez
//CST 227

using GameCell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Driver
{
    class Program
    {
        static MinesweeperGame MinesweeperGrid = new MinesweeperGame(9);

        static void Main(string[] args)
        {
            MinesweeperGrid.activateCells();
            MinesweeperGrid.countLive();
            MinesweeperGrid.countMines();

            while (true)
            {
                MinesweeperGame.printGrid(MinesweeperGrid);
                MinesweeperGrid.playGame();

            }
        }
    }
}
