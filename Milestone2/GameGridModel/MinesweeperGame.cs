﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameCell
{
    public class MinesweeperGame : Grid, IPlayable
    {
        static int mines = 0;
        static int movesleft = 81;
        public MinesweeperGame()
        {
        }

        public MinesweeperGame(int s)
        {
            this.Size = s;

            gameGrid = new Cell[this.Size, this.Size];
            for (int i = 0; i < this.Size; i++)
            {
                for (int j = 0; j < this.Size; j++)
                {
                    gameGrid[i, j] = new Cell(i, j);
                }
            }
        }



        static public void printGrid(MinesweeperGame minesweeperGrid)
        {
            //prints grid
            for (int i = 0; i < minesweeperGrid.Size; i++)
            {
                for (int j = 0; j < minesweeperGrid.Size; j++)
                {
                    if (minesweeperGrid.gameGrid[i, j].beenVisited && (minesweeperGrid.gameGrid[i, j].liveNeighbors == 0))
                    {
                        Console.Write(" ~ "); // the text printed for the hidden board  
                    }
                    else if (minesweeperGrid.gameGrid[i, j].beenVisited)
                    {
                        Console.Write(" " + minesweeperGrid.gameGrid[i, j].liveNeighbors + " ");
                    }
                    else
                    {
                        Console.Write(" ? ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("=====================================");
        }

        public void checkLive(int row, int column) // checks if you hit a mine otherwise it will tag the space as visited
        {
            if (gameGrid[row, column].isLive)
            {
                Console.Clear();
                revealGrid();
                Console.WriteLine("************** GAME OVER **************");
                Console.ReadKey();
                Environment.Exit(0);
            }
            else
            {
                gameGrid[row, column].beenVisited = true;
            }
        }

        public override void revealGrid()
        {
            //prints grid
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (gameGrid[i, j].isLive) // if the cell is live it will print a "*"
                    {
                        Console.Write(" * ");
                    }

                    else
                    {
                        Console.Write(" " + gameGrid[i, j].liveNeighbors + " "); // if the cell is not live it will print its number of neighbors that are live.
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("=====================================");
        }


        public override void activateCells()
        {
            // activates random cells to contain mines with a 20% chance of activating
            var random = new Random();
            var rng = random.Next(0, 1);
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    if (random.NextDouble() < 0.2)
                    {
                        //set live
                        gameGrid[r, c].isLive = true;
                        gameGrid[r, c].liveNeighbors = 9;
                    }
                }
            }

        }

        public void countMines() // reduced the moves needed to win based on the number of mines on the board
        {
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    if (gameGrid[r,c].isLive)
                    {
                        --movesleft;
                    }
                }
            }
        }

        public void countLive()
        {
            //takes each cell and looks at the cell next to it to see if it is live. If that cell is live it will add 1 point to the currentcell.
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    if (r == 0 && c == 0) // prevents out of bounds for the element 0,0
                    {
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }

                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (r == Size - 1 && c == Size - 1) // prevents out of bounds for the element found at the end of the array
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (r == 0) // prevents out of bounds at the beginning of each array
                    {
                        if (c != Size - 1)
                        {
                            if (gameGrid[r, c + 1].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                            if (gameGrid[r + 1, c + 1].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                        }
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (c == 0) // prevents out of bounds in the top/first array
                    {
                        if (r != Size - 1)
                        {
                            if (gameGrid[r + 1, c].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                            if (gameGrid[r + 1, c + 1].isLive == true)
                            {
                                ++gameGrid[r, c].liveNeighbors;
                            }
                        }
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (r == Size - 1) // prevents out of bounds at the end of each array
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else if (c == Size - 1) // prevents out of bounds at the bottom/last array
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                    else // no issues about out of bounds
                    {
                        if (gameGrid[r, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r - 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c + 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                        if (gameGrid[r + 1, c - 1].isLive == true)
                        {
                            ++gameGrid[r, c].liveNeighbors;
                        }
                    }
                }
            }
        }

        public void playGame()
        {

            if (movesleft == 0) // the win condition for the game. If you complete all of your moves without activating the reveal method you automatically win
            {
                Console.Clear();
                revealGrid();
                Console.WriteLine("************** YOU WIN **************");
                Console.ReadKey();
                Environment.Exit(0);
            }
            Console.WriteLine("What row do you want to choose? {0-9}");
            int row = int.Parse(Console.ReadLine());

            Console.WriteLine("What column do you want to choose? {0-9}");
            int column = int.Parse(Console.ReadLine());

            checkLive(row, column);
            --movesleft;
            Console.Clear();
        }
    }
}
