﻿// Tyler Martinez
//CST 227

using System;
using System.Collections.Generic;
using System.Text;

namespace GameCell
{
    public abstract class Grid
    {
        public int Size { get; set; }

        public Cell[,] gameGrid;

        public Grid()
        {
        }

        public Grid(int s)
        {
            Size = s;

            gameGrid = new Cell[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    gameGrid[i, j] = new Cell(i, j);
                }
            }
        }

        public abstract void activateCells();

        public abstract void revealGrid();
    }
}