﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChessBoardModel
{
    public class Board
    {
        public int Size { get; set; }

        public Cell[,] theGrid;

        public Board(int s)
        {
            Size = s;

            theGrid = new Cell[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    theGrid[i, j] = new Cell(i, j);
                }
            }
        }

        public void MarkNextLegalMove(Cell currentCell, string chessPiece)
        {
            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    theGrid[r, c].LegalNextMove = false;
                    theGrid[r, c].CurrentlyOccupied = false;
                }
            }

            switch (chessPiece)
            {
                case "Knight":
                case "knight":
                    
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -2, -1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -2, +1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -1, -2);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -1, +2);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +2, -1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +2, +1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +1, -2);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +1, +2);
                    theGrid[currentCell.RowNumber, currentCell.ColumnNumber].CurrentlyOccupied = true;
                    break;

                case "King":
                case "king":
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -1, -1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -1, +0);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -1, +1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +0, -1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +0, +1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +1, -1);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +1, +0);
                    boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +1, +1);
                    theGrid[currentCell.RowNumber, currentCell.ColumnNumber].CurrentlyOccupied = true;
                    break;

                case "Rook":
                case "rook":
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], 0, -i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -i, 0);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +i, 0);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], 0, +i);
                    }
                    theGrid[currentCell.RowNumber, currentCell.ColumnNumber].CurrentlyOccupied = true;
                    break;

                case "Bishop":
                case "bishop":
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -i, -i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -i, +i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +i, -i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +i, +i);
                    }
                    theGrid[currentCell.RowNumber, currentCell.ColumnNumber].CurrentlyOccupied = true;
                    break;

                case "Queen":
                case "queen":
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], 0, -i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -i, 0);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +i, 0);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], 0, +i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -i, -i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], -i, +i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +i, -i);
                    }
                    for (int i = 1; i < Size + 1; i++)
                    {
                        boundsChecker(theGrid[currentCell.RowNumber, currentCell.ColumnNumber], +i , +i);
                    }
                    theGrid[currentCell.RowNumber, currentCell.ColumnNumber].CurrentlyOccupied = true;
                    break;

                default:
                    break;
            }
        }

        public void boundsChecker(Cell currentCell, int row, int column)
        {
            if ((currentCell.RowNumber + row) < 0 || (currentCell.RowNumber + row) >= Size)
            {
                //issues
            }
            else if ((currentCell.ColumnNumber + column) < 0 || (currentCell.ColumnNumber + column )>= Size)
            {
                //issues
            }
            else
            {
                theGrid[currentCell.RowNumber + row, currentCell.ColumnNumber + column].LegalNextMove = true;
            }
        } 
    }
}
