﻿using ChessBoardModel;
using System;

namespace ChessBoardConsoleApp
{
    class Program
    {
        static Board myBoard = new Board(33);

        static void Main(string[] args)
        {


            //show the empty chess board
            printGrid(myBoard);

            //get the location of the chess piece
            while (true)
            {

                //calculate and mark the cells where legal moves are possible.
                Console.WriteLine("What piece do you want to use?\n{Knight}\n{King}\n{Rook}\n{Bishop}\n{Queen}\n{Exit}");
                string piece = Console.ReadLine();

                if ((piece == "Exit") || (piece == "exit"))
                {
                    System.Environment.Exit(1);
                }

                Cell myLocation = setCurrentCell();
                myBoard.MarkNextLegalMove(myLocation, piece);
                
                //show the chess board.  Use. for an empty square, X for the piece location and + for a possible legal move
                printGrid(myBoard);
                myLocation.CurrentlyOccupied = false;
            }
            //wait for another return key to end the program
           // Console.ReadLine();
        }

        static public void printGrid(Board myBoard)
        {
            //print board in console. X current location. + legal move. and (.) for an empty square.
            for (int i = 0; i < myBoard.Size-16; i++)
            {
                for (int j = 0; j < myBoard.Size; j++)
                {


                    if ((i % 6 == 0 || i%2==0)&& j % 4 == 0)
                    {
                        Console.Write("+");
                    }

                    if ((i % 6 > 0 && i%2 != 0)&& j % 4 == 0)
                    {
                        Console.Write("|");
                    }

                    if ((i % 6 == 0 || i%2==0)&& j % 4 > 0)
                    {
                        Console.Write("-");
                    }

                    if (i % 2 > 0 && j % 4 > 0)
                    {
                        if (myBoard.theGrid[i, j].CurrentlyOccupied)
                        {
                            Console.Write("X");
                        }
                        else if (myBoard.theGrid[i, j].LegalNextMove)
                        {
                            Console.Write("O");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("=====================================");
        }

        static public Cell setCurrentCell()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Enter your current row number");
                    int currentRow = int.Parse(Console.ReadLine());
                    int rowCalc = currentRow + currentRow + 1;

                    Console.WriteLine("Enter your current column number");
                    int currentColumn = int.Parse(Console.ReadLine());
                    int colCalc = currentColumn + currentColumn;
                    colCalc = (colCalc + colCalc) + 2;

                    myBoard.theGrid[rowCalc, colCalc].CurrentlyOccupied = true;
                    return myBoard.theGrid[rowCalc, colCalc];
                }
                catch
                {
                    Console.WriteLine("Error: Numbers out of range. Enter a number between 0 and 7.");

                }
            }

        }
    }
}
