﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarShopGUI
{
    public partial class Form1 : Form
    {

        Store store = new Store();

        BindingSource carListBinding = new BindingSource();
        BindingSource ShoppingListBinding = new BindingSource();


        public Form1()
        {
            InitializeComponent();
            SetBindings();
        }

        private void SetBindings()
        {
            carListBinding.DataSource = store.CarList;
            listBox1.DataSource = carListBinding;
            listBox1.DisplayMember = "Display";
            listBox1.ValueMember = "Display";

            ShoppingListBinding.DataSource = store.ShoppingList;
            listBox2.DataSource = ShoppingListBinding;
            listBox2.DisplayMember = "Display";
            listBox2.ValueMember = "Display";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try //A try catch to make sure that the correct values are being inputed into the create a car box.
            {
                Car newCar = new Car();
                newCar.Make = textBox1.Text;
                newCar.Model = textBox2.Text;
                newCar.Price = Decimal.Parse(textBox3.Text);
                newCar.Year = int.Parse(textBox4.Text);
                newCar.Color = textBox5.Text;

                store.CarList.Add(newCar);

                carListBinding.ResetBindings(false);
            }
            catch
            {
                MessageBox.Show("Please enter a correct value!");
            }



        }

        private void button2_Click(object sender, EventArgs e)
        {
                store.ShoppingList.Add((Car)listBox1.SelectedItem); //adds the selceted car from the car list to the shopping list

                ShoppingListBinding.ResetBindings(false);
            // there is an error where if you try to add to the shopping list when nothing is in the car store it will cause 
            // an error where you cannot remove the hidden value.
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                decimal total = store.checkout(); //calculates the total price of the cars in the shopping list.
                label7.Text = total.ToString();
            }
            catch
            {
                MessageBox.Show("Error: Please add an item to your shopping cart.");
            }
        }
    }
}
